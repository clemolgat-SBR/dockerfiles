# Dockerfile C++/CMake/QiBuild Development Environment

* [ubuntu:latest](https://hub.docker.com/_/ubuntu/)
* [GCC/G++ 4.5.2](http://packages.ubuntu.com/xenial/build-essential)
* [CMake 3.5.1](http://packages.ubuntu.com/xenial/cmake)
* [Pip 8.1.1-2](http://packages.ubuntu.com/xenial/python-pip)
* GStreamer-0.10 with base and good plugins

## Docker Build

```sh
docker build -t clemolgatsbr/qibuild .
```

## Run
To run docker with X11 forwarding.  
On host retrieve credential:
```sh
xauth list
clemolgat-sbr/unix:0  MIT-MAGIC-COOKIE-1  abadebabe1337badecafedeadbeef404
```

Then run image with:
```sh
docker run --net=host -e DISPLAY -it clemolgatsbr/qibuild bash
```
Once inside the image
```sh
xauth add clemolgat-sbr/unix:0  MIT-MAGIC-COOKIE-1  abadebabe1337badecafedeadbeef404
```

Now you can use GUI e.g.:
```sh
gst-launch-0.10 -v tcpclientsrc host=10.0.165.4 port=3001 ! multipartdemux ! jpegdec ! autovideosink
```
note: use fakesink if you don't want display
